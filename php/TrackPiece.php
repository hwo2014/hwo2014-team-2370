<?php

class TrackPiece {
    private $length;
    private $switch;
    private $radius;
    private $angle;
    private $idx;
    private $bestLane;

    public function __construct($piece, $idx) {
        if (isset($piece['length']))
            $this->length = $piece['length'];
        else
            $this->length = 0;

        if (isset($piece['switch']))
            $this->switch = $piece['switch'];
        else 
            $this->switch = false;

        if (isset($piece['radius']))
            $this->radius = $piece['radius'];
        else 
            $this->radius = 0;

        if (isset($piece['angle']))
            $this->angle = $piece['angle'];
        else
            $this->angle = 0;

        if ($this->length == 0) {
            $this->length = abs((($this->radius * 3.14) / 180) * $this->angle);
        }

        $this->idx = $idx;
    }

    public function getLength($percent = null) {
        if (!is_null($percent)) {
            return ($this->length * ($percent / 100));
        }

        return $this->length;
    }

    public function setBestLane($best) {
        $this->bestLane = $best;
    }

    public function getBestLane() {
        return $this->bestLane;
    }

    public function getSwitch() {
        return $this->switch;
    }

    public function getRadius() {
        return $this->radius;
    }

    public function getAngle() {
        return $this->angle;
    }

    public function getIdx() {
        return $this->idx;
    }

    public function isStraight() {
        if ($this->radius == 0 && $this->angle == 0)
            return true;

        return false;
    }

    public function isSwitch() {
        if ($this->switch == false)
            return false;

        return true;
    }

    public function __toString() {
        $obj  = "Length : " . $this->length . "\n";
        $obj .= "Switch : " . $this->switch . "\n";
        $obj .= "Angle  : " . $this->angle . "\n";    
        $obj .= "Radius : " . $this->radius . "\n\n";

        return $obj;
    }    

}
