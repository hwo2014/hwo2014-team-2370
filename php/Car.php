<?php
require_once 'Position.php';

class Car {
    //Car properties
    private $name;
    private $key;
    private $color;    
    private $length;
    private $width;
    private $flagPosition;

    //Car Position
    private $currentPosition;
    private $lastPosition;

    private $speed;
    private $throttle;
    private $laneChangeRequest;
    private $angleChange;

    public function __construct($name, $key) {
        $this->name = $name;
        $this->key = $key;
        $this->speed = 0;
        $this->throttle = 0;
        $this->laneChangeRequest = false;

        $this->currentPosition = new Position();
        $this->lastPosition = new Position();
    }

    public function __destruct() {
        unset($this->currentPosition);
        unset($this->lastPosition);
    }

    public function join() {
        Connection::write_msg('join', array(
            'name' => $this->name,
            'key' => $this->key
        ));
    }

    public function init($data) {
        if ($this->name != $data['name'])
            throw new Exception('Bot name mismatch',1);

        if (empty($data['color']))
            throw new Exception('Bot color property not exists',2);

        $this->color = $data['color'];
    }

    public function updatePosition($data, $track) {           
        //Copy current position into last position
        $this->lastPosition->copy($this->currentPosition);

        //Update current position
        $data['piecePosition']['angle'] = $data['angle'];
        $this->currentPosition->update($data['piecePosition'], $track);

        //Calculate the speed
        $dist = $this->currentPosition->absoluteDistance - $this->lastPosition->absoluteDistance;
        $time = $this->currentPosition->time - $this->lastPosition->time;

        $this->speed = round($dist / $time);

        //Calculate angle change
        $this->angleChange = abs($this->currentPosition->angle - $this->lastPosition->angle);
    }

    public function crash($data) {
        if ($this->name == $data['name'] && $this->color == $data['color'])
            throw new Exception("Car crashed", 7);
    }

    public function setLane() {
        if ($this->currentPosition->bestLane < $this->currentPosition->endLane) {
            if ($this->laneChangeRequest != 'l') {
                echo "Change to LEFT";
                $this->laneChangeRequest = 'l';
                Connection::write_msg('switchLane', 'Left');                
            }
        } else if ($this->currentPosition->bestLane > $this->currentPosition->endLane) {
            if ($this->laneChangeRequest != 'r') {
                echo "Change to RIGHT";
                $this->laneChangeRequest = 'r';
                Connection::write_msg('switchLane', 'Right');                
            }
        }
    }

    public function getPiece() {
        return $this->currentPosition->pieceIndex;
    }

    public function getInPiecePosition() {
        return $this->currentPosition->inPieceDistance;
    }

    public function danger() {
        if ($this->angleChange > 2.5)
            return true;

        return false;
    }

    public function angleIncreasing() {        
        $change = abs($this->currentPosition->angle) - abs($this->lastPosition->angle);

        if ($this->angleChange > 0)
            return true;

        return false;
    }

    public function angleDecreasing() {    
        $change = abs($this->lastPosition->angle) - abs($this->currentPosition->angle);

        if ($change > 0)
            return true;

        return false;
    }

    public function getAngle() {
        return abs($this->currentPosition->angle);
    }

    public function getColor() {
        return $this->color;
    }

    public function getName() {
        return $this->name;
    }

    public function getSpeed() {
        return $this->speed;
    }

    public function getThrottle() {
        return $this->throttle;
    }

    public function setSpeed($speed) {
        if ($speed < 0)
            $this->throttle = 0;
        else if ($speed > 1)
            $this->throttle = 1;
        else
            $this->throttle = round($speed,2);

        Connection::write_msg('throttle', $this->throttle);
    }

    public function increaseSpeed($inc) {
        $this->setSpeed($this->throttle + $inc);
    }

    public function decreaseSpeed($dec) {
        $this->setSpeed($this->throttle - $dec);
    }

    public function keepSpeed() {
        Connection::write_msg('throttle', $this->throttle);
    }

    public function __toString() {
        $obj  = "Name          : " . $this->name . "\n";
        $obj .= "Speed         : " . $this->speed . "\n";
        $obj .= "Throttle      : " . $this->throttle . "\n";
        $obj .= "Angle change  : " . $this->angleChange . "\n";

        if ($this->laneChangeRequest === false)
            $obj .= "Lane Change   : No\n";
        else 
            $obj .= "Lane Change   : ". $this->laneChangeRequest ."\n";

        /*$obj .= "Color   : " . $this->color . "\n";    
        $obj .= "Length  : " . $this->length . "\n";
        $obj .= "Width   : " . $this->width . "\n";
        $obj .= "flagPos : " . $this->flagPosition . "\n\n";*/

        //Car Position
        $obj .= $this->currentPosition->__toString();

        return $obj;
    }    
}
