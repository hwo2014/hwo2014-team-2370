<?php
require_once 'Track.php';
require_once 'Car.php';

class Race {
    public $track;
    public $myCar;
    private $tick;

    private $straightCounter;
    private $raw_cars;

    public function __construct($botname, $botkey) {
        //Create my Car object
        $this->myCar = new Car($botname, $botkey);
        $this->myCar->join();
    }

    public function init($data) {
        $this->track = new Track($data['race']['track']);
        $this->raw_cars = $data['race']['cars'];
/*"msgType":"gameInit","data":{

    "race":{"track":{"id":"keimola","name":"Keimola",

    "startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},
    "cars":[{"id":{"name":"WireSpider","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"durationMs":20000}}},"gameId":"a7c3ed50-d4ab-4879-b8c8-69f062ea499b"}

    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }
  }
}}*/
    }

    public function gameStart() {
/*{"msgType": "gameStart", "data": null}*/
    }

    public function gameEnd() {
/*{"msgType": "gameEnd", "data": {
  "results": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "laps": 3,
        "ticks": 9999,
        "millis": 45245
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ],
  "bestLaps": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "lap": 2,
        "ticks": 3333,
        "millis": 20000
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ]
}}*/
    }

    public function newLap() {
/*{"msgType": "lapFinished", "data": {
  "car": {
    "name": "Schumacher",
    "color": "red"
  },
  "lapTime": {
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  },
  "raceTime": {
    "laps": 1,
    "ticks": 666,
    "millis": 6660
  },
  "ranking": {
    "overall": 1,
    "fastestLap": 1
  }
}, "gameId": "OIUHGERJWEOI", "gameTick": 300}*/
    }

    public function finish() {
/*{"msgType": "finish", "data": {
  "name": "Schumacher",
  "color": "red"
}, "gameId": "OIUHGERJWEOI", "gameTick": 2345}*/
    }

    public function updatePosition($data, $tick) {
        $this->tick = $tick;

        foreach ($data as $carPosition) {
            if (
                $carPosition['id']['color'] == $this->myCar->getColor() &&
                $carPosition['id']['name'] == $this->myCar->getName()
            ) {
                $this->myCar->updatePosition($carPosition, $this->track);
            }
        }

        //throw new Exception('Car not found error',3);
    }

    public function getCurrentPiece() {
        $idx = $this->myCar->getPiece();
        return $this->track->getPiece($idx);
    }

    public function getNextPiece() {
        $idx = $this->myCar->getPiece();
        return $this->track->getPiece($idx+1);
    }

    public function getTick() {
        return $this->tick;
    }

    public function cornerIsComing2() {        
        $currentPiece = $this->getCurrentPiece();
        $nextPiece = $this->getNextPiece();
        
        $idx = $this->myCar->getPiece();        
        $next_next = $this->track->getPiece($idx+2);

        if ($nextPiece->isStraight() === true && $currentPiece->isStraight() === true && $next_next->isStraight() === false)
            return true;

        return false;
    }

    public function cornerIsComing() {
        $currentPiece = $this->getCurrentPiece();
        $nextPiece = $this->getNextPiece();

        if ($nextPiece->isStraight() === false && $currentPiece->isStraight() === true)
            return true;

        return false;
    }

    public function isEndOfCorner() {
        $currentPiece = $this->getCurrentPiece();
        $nextPiece = $this->getNextPiece();

        if ($nextPiece->isStraight() === true && $currentPiece->isStraight() === false)
            return true;

        return false;
    }

    public function isCorner() {
        $currentPiece = $this->getCurrentPiece();

        if ($currentPiece->isStraight() === false)
            return true;

        return false;
    }

    public function isStraight() {
        $currentPiece = $this->getCurrentPiece();
        return $currentPiece->isStraight();
    }
}
