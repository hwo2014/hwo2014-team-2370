<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

class Connection {
	private static $sock;
	private static $debug_enabled;

	public static function connect($host, $port, $botkey) {
		self::$sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if (self::$sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect(self::$sock, $host, $port)) {
			throw new Exception($host . ': ' . self::sockerror());
		}
	}

	public static function close() {
        if (isset(self::$sock)) {
            socket_close(self::$sock);
        }
	}

	public static function read_msg() {
		$line = @ socket_read(self::$sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			self::debug('** ' . self::sockerror());
		} else {
			self::debug('<= ' . rtrim($line));
		}
		return json_decode($line, TRUE);
	}

	public static function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		self::debug('=> ' . rtrim($str));
		if (@ socket_write(self::$sock, $str) === FALSE) {
			throw new Exception('write: ' . self::sockerror());
		}
	}
	
	public static function sockerror() {
		return socket_strerror(socket_last_error(self::$sock));
	}
	
	public static function setDebug($enabled) {
		self::$debug_enabled = $enabled;
	} 

	private static function debug($msg) {
		if (self::$debug_enabled === true) {
			echo $msg, "\n";
		}
	}	
}
?>