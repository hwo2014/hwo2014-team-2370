<?php

class TrackLane {
    private $distanceFromCenter;
    private $index;

    public function __construct($lane) {
        $this->distanceFromCenter = $lane['distanceFromCenter'];
        $this->index = $lane['index'];
    }

    public function getDistance() {
        return $this->distanceFromCenter;
    }

    public function getIndex() {
        return $this->index;
    }    

    public function __toString() {
        $obj  = "Index                    : " . $this->index . "\n";
        $obj .= "Distance From Center     : " . $this->distanceFromCenter . "\n";

        return $obj;
    }    

}
