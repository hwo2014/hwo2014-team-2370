<?php

require_once 'Connection.php';
require_once 'Race.php';

define("MIN_ANGLE", 40);
define("MAX_ANGLE", 50);
define("MIN_SPEED_BEFORE_CORNER", 45);
define("MAX_SPEED_BEFORE_CORNER", 60);
define("MAX_SPEED_BEFORE_CORNERISCOMING", 100);
define("DEBUG", true);

class myBot {
    private $race;

    public function __construct ($host, $port, $botname, $botkey, $debug = DEBUG) {
        Connection::setDebug($debug);
        Connection::connect($host, $port, $botkey);

        $this->race = new Race($botname, $botkey);

        if (!$this->race)
            throw new Exception("Race object not created", 10);
    }

    function __destruct() {
        Connection::close();
    }

    public function test() {
        $json = '{"msgType":"yourCar","data":{"name":"WireSpider","color":"purple"},"gameId":"c18cb94c-c9ec-46fc-9ef6-fde2b8bcb819"}';
        $msg = json_decode($json, TRUE);
        $this->race->myCar->init($msg['data']);

        $json = '{"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"Traffic-5","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}},{"id":{"name":"Traffic-1","color":"yellow"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}},{"id":{"name":"WireSpider","color":"purple"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}},{"id":{"name":"Traffic-3","color":"green"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}},{"id":{"name":"Traffic-4","color":"blue"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}},{"id":{"name":"Traffic-2","color":"orange"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"durationMs":40000}}},"gameId":"c18cb94c-c9ec-46fc-9ef6-fde2b8bcb819"}';
        $msg = json_decode($json, TRUE);

        $this->race->init($msg['data']);
        echo $this->race->track;

        $json = '{"msgType":"carPositions","data":[{"id":{"name":"Traffic-5","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}},{"id":{"name":"Traffic-4","color":"blue"},"angle":0.0,"piecePosition":{"pieceIndex":33,"inPieceDistance":79.48968840982664,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":-1}},{"id":{"name":"Traffic-1","color":"yellow"},"angle":0.0,"piecePosition":{"pieceIndex":26,"inPieceDistance":45.13864217863252,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":-1}},{"id":{"name":"Traffic-3","color":"green"},"angle":0.0,"piecePosition":{"pieceIndex":19,"inPieceDistance":64.49555921538756,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":-1}},{"id":{"name":"Traffic-2","color":"orange"},"angle":0.0,"piecePosition":{"pieceIndex":12,"inPieceDistance":63.94099050085663,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":-1}},{"id":{"name":"WireSpider","color":"purple"},"angle":0.0,"piecePosition":{"pieceIndex":6,"inPieceDistance":10.510311590173274,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":-1}}],"gameId":"c18cb94c-c9ec-46fc-9ef6-fde2b8bcb819"}';
        $msg = json_decode($json, TRUE);

        if (!isset($msg['gameTick']))
            $msg['gameTick'] = 0;

        $this->race->updatePosition($msg['data'], $msg['gameTick']);
        echo $this->race->myCar;
    }

    public function run() {
        
        //$this->test();

        while (!is_null($msg = Connection::read_msg())) {
            switch ($msg['msgType']) {
                case 'carPositions':
                    
                    if (!isset($msg['gameTick']))
                        $msg['gameTick'] = 0;

                    $this->race->updatePosition($msg['data'], $msg['gameTick']);

                    echo $this->race->myCar;

                    $this->race->myCar->setLane();                    

                    //Set throttle                    
/*                    if ($this->race->cornerIsComing2() && $this->race->myCar->getSpeed() > MAX_SPEED_BEFORE_CORNERISCOMING) {
                        echo "State : Corner2 is coming, slow down\n";
                        $this->race->myCar->decreaseSpeed(0.2);
                    } else 
*/                    if ($this->race->cornerIsComing() && $this->race->myCar->getSpeed() > MAX_SPEED_BEFORE_CORNER) {
                        echo "State : Corner is coming, slow down\n";
                        $this->race->myCar->decreaseSpeed(0.25);
                    } else if ($this->race->cornerIsComing() && $this->race->myCar->getSpeed() < MIN_SPEED_BEFORE_CORNER) {
                        echo "State : Corner is coming, more speed\n";
                        $this->race->myCar->IncreaseSpeed(0.3);
                    } else if ($this->race->myCar->getAngle() > MIN_ANGLE && $this->race->myCar->angleIncreasing() === true) {
                        echo "State : Corner slow down\n";
                        $this->race->myCar->setSpeed(0);
                    } else if ($this->race->myCar->getAngle() < MAX_ANGLE && $this->race->myCar->angleDecreasing() === true) {
                        echo "State : Corner speed up\n";
                        $this->race->myCar->increaseSpeed(0.3);
                    } else if ($this->race->myCar->danger()) {
                        echo "State : Danger, stop\n";
                        $this->race->myCar->setSpeed(0);
                    } else if ($this->race->myCar->getSpeed() == 0 || $this->race->isEndOfCorner() || $this->race->isStraight()) {
                        echo "State : Car is stoped || EndOfCorner || Straight\n";
                        $this->race->myCar->setSpeed(1);
                    } else {
                        echo "State : Keep speed\n";
                        $this->race->myCar->keepSpeed();
                    }

                    break;
                case 'yourCar':
                    $this->race->myCar->init($msg['data']);
                    break;
                case 'gameInit':
                    $this->race->init($msg['data']);
                    //echo $this->race->track;
                    //exit;
                    break;
                case 'gameStart':
                    //Start the race
                    $this->race->gameStart();
                    $this->race->myCar->setSpeed(1);
                    break;
                case 'lapFinished':
                    $this->race->newLap();
                    break;
                case 'gameEnd':
                    $this->race->gameEnd($msg['data']);
                    break;
                case 'crash':
                    $this->race->myCar->crash($msg['data']);
                    break;
                case 'finish':
                    $this->race->finish($msg['data']);
                    break;
                case 'tournamentEnd':
                    //Print out results

                    //Destroy my Car
                    unset($this->myCar);

                    //Destroy the race
                    unset($this->race);

                    //Close connection
                    //Connection::close();
                    //exit;
                    break;
                case 'dnf':
                    throw new Exception("Car was disqualified", 9);
                    break;
                default:
                    Connection::write_msg('ping', null);
            }
        }
    }    
}