<?php

class Position {
    public $angle;
    public $pieceIndex;
    public $inPieceDistance;
    public $startLane;
    public $endLane;
    public $lap;
    public $absoluteDistance;
    public $time;

    //Best lane calculation
    public $bestLane;
    private $nextSwitch;

    public function update($position, $track) {
        $this->angle = $position['angle'];
        $this->pieceIndex = $position['pieceIndex'];
        $this->inPieceDistance = $position['inPieceDistance'];
        $this->startLane = $position['lane']['startLaneIndex'];
        $this->endLane = $position['lane']['endLaneIndex'];
        $this->lap = $position['lap'];
        $this->absoluteDistance = $track->getDistance($this->lap, $this->pieceIndex, $this->inPieceDistance);
        $this->time = microtime(true);

        $this->calculateLane($track, $this->pieceIndex);
    }

    private function calculateLane($track, $idx) {
        if ($idx > 0)
            $pieces_part = array_slice($track->pieces, $idx);
        else 
            $pieces_part = $track->pieces;

        //Search for next switch
        foreach ($pieces_part as $piece) {
            if ($piece->isSwitch()) {
                $this->nextSwitch = $piece->getBestLane();
                break;
            }
        }

        //Search best lane
        $pos = null;
        foreach ($track->lanes as $lane) {
            if ($this->nextSwitch == 'Right') {
                if (is_null($pos) || $pos < $lane->getDistance()) {
                    $pos = $lane->getDistance();
                    $this->bestLane = $lane->getIndex();
                }
            } else {
                if (is_null($pos) || $pos > $lane->getDistance()) {
                    $pos = $lane->getDistance();
                    $this->bestLane = $lane->getIndex();
                }                
            }
        }

    }

    public function copy($position) {
        $this->angle = $position->angle;
        $this->pieceIndex = $position->pieceIndex;
        $this->inPieceDistance = $position->inPieceDistance;
        $this->startLane = $position->startLane;
        $this->endLane = $position->endLane;
        $this->lap = $position->lap;
        $this->absoluteDistance = $position->absoluteDistance;
        $this->time = $position->time;
        $this->bestLane = $position->bestLane;
        $this->nextSwitch = $position->nextSwitch;
    }

    public function __toString() {
        //Car Position
        $obj  = "Angle         : " . $this->angle . "\n";
        $obj .= "pieceIdx      : " . $this->pieceIndex . "\n";
        $obj .= "inPieceDist   : " . $this->inPieceDistance . "\n";
        $obj .= "startLane     : " . $this->startLane . "\n";
        $obj .= "endLane       : " . $this->endLane . "\n";
        $obj .= "Lap           : " . $this->lap . "\n";
        $obj .= "Absolute dist : " . $this->absoluteDistance . "\n";
        $obj .= "Time          : " . $this->time . "\n";
        $obj .= "Best Lane     : " . $this->bestLane . "\n";
        $obj .= "Next Switch   : " . $this->nextSwitch . "\n";

        return $obj;
    }

}