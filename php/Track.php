<?php
require_once 'TrackPiece.php';
require_once 'TrackLane.php';

class Track {
    public $pieces;
    public $lanes;
    private $raw_pieces;
    private $raw_lanes;
    private $tracklength;

    private $id;
    private $name;

    public function __construct($track) {
        $this->raw_pieces = $track['pieces'];
        $this->raw_lanes = $track['lanes'];
        $this->id = $track['id'];
        $this->name = $track['name'];

        $idx = 0;
        $this->tracklength = 0;
        foreach ($this->raw_pieces as $piece) {
            $this->pieces[] = new TrackPiece($piece, $idx++);

            //Calculate the length
            $piece = $this->getPiece($idx - 1);
            $this->tracklength += $piece->getLength();
        }

        foreach ($this->raw_lanes as $lane) {
            $this->lanes[] = new TrackLane($lane);
        }

        $this->analizeTrack();
    }

    private function analizeTrack() {
        $switches = array();
        $switch_idx = 0;

        //Collect information about switches
        foreach ($this->pieces as $piece) {
            if ($piece->isSwitch() && !isset($switches[$switch_idx]['start'])) {
                $switches[$switch_idx]['start'] = $piece->getIdx();
            } else if ($piece->isSwitch() && isset($switches[$switch_idx]['start'])) {
                $switches[$switch_idx]['stop'] = $piece->getIdx();
                $switch_idx++;
                $switches[$switch_idx]['start'] = $piece->getIdx();
            }
        }

        if (!isset($switches[$switch_idx]['stop']))
            $switches[$switch_idx]['stop'] = $switches[$switch_idx]['start'];

        //Check the corners between the switches
        foreach ($switches as $switch) {
            $left = 0;
            $right = 0;
            for ($idx = $switch['start']; $idx <= $switch['stop']; $idx++) {
                $piece = $this->getPiece($idx);

                if (!$piece->isStraight()) {
                    if ($piece->getAngle() < 0)
                        $left += $piece->getLength();
                    else
                        $right += $piece->getLength();
                }
            }

            //Mark switch with the bestLane
            if ($left > $right) {
                $this->setBestLane($switch['start'], 'Left');
            } else {
                $this->setBestLane($switch['start'], 'Right');
            }
        }
    }

    public function getSwitch($idx) {
        $piece = $this->getPiece($idx);
        return $piece->isSwitch();
    }

    public function getDistance($lap, $idx, $inPieceDistance) {
        if ($lap < 0)
            $lap = 0;

        $length = $lap * $this->tracklength;

        foreach ($this->pieces as $piece) {
            //Stop at the index
            if ($piece->getIdx() === $idx) {
                $length += $piece->getLength($inPieceDistance);
                break;
            }

            $length += $piece->getLength();
        }

        return $length;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setBestLane($idx, $bestLane) {
        if ($idx >= count($this->pieces))
            $idx = 0;
    
        $this->pieces[$idx]->setBestLane($bestLane);
    }

    public function getPiece($idx) {
        if ($idx >= count($this->pieces))
            $idx = 0;

        return $this->pieces[$idx];
    }

    public function __toString() {
        $obj = "Pieces --------------------\n";

        foreach ($this->pieces as $piece) {
            $obj .= $piece->__toString();
        }

        $obj .= "\nLanes ---------------------\n";

        foreach ($this->lanes as $lane) {
            $obj .= $lane->__toString();
        }

        return $obj;
    }
}
